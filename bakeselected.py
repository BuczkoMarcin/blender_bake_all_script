#
# Script makes baking for selected object. 
#
# Athor: Marcin Buczko
# Date: 2016.11.15
#
# Problems: - sometimes script produces only black image as baking result. 
#             In such situation manual baking any object brings script on working again 
#           - if destination image for baking exists but not initialized then script ends with error

import bpy
import os


def bake_selected():
    # get object
    obj = bpy.context.object

    # parameters
    random_number      = "1238734563"
    baked_textures_dir =  bpy.path.abspath("//baked_textures")
    image_name         = "baked_uv_" + obj.name
    texture_node_name  = "baked_uv_texture" + random_number
    baked_uv_name      = "baked_uv"
    image_width        = 2048
    image_height       = 2048

    # constants
    modes = {'EDIT_MESH' : 'EDIT', 'OBJECT' : 'OBJECT', 'PAINT_WEIGHT' : 'WEIGHT_PAINT', 'PAINT_TEXTURE' : 'TEXTURE_PAINT', 'PAINT_VERTEX' : 'VERTEX_PAINT', 'SCULPT' : 'SCULPT'}

    

    # create image for baked uv
    image = (bpy.data.images.get(image_name) or 
            bpy.data.images.new(name=image_name,width = image_height,height = image_height))

    # add ShaderNodeTexImage to all materials connected to object
    # with image as texture map

    for material in obj.data.materials:
        material.use_nodes = True
        nodes = material.node_tree.nodes
        texture_node = (nodes.get(texture_node_name) or
                       nodes.new('ShaderNodeTexImage'))
        texture_node.label = texture_node_name
        texture_node.name  = texture_node_name
        texture_node.image = image
        texture_node.select = True
        nodes.active = texture_node

    # add UV map with smart unwraping
    old_active_uv = obj.data.uv_textures.active
    old_active_index = obj.data.uv_textures.active_index

    bake_uv = (obj.data.uv_textures.get(baked_uv_name) or
               obj.data.uv_textures.new(baked_uv_name))
    obj.data.uv_textures.active = bake_uv


    old_mode = bpy.context.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
    bpy.ops.object.mode_set(mode = 'EDIT')
    bpy.ops.mesh.select_all(action = 'SELECT')
    bpy.ops.uv.smart_project(island_margin=0.01)
    bpy.ops.mesh.select_all(action = 'DESELECT')
    bpy.ops.object.mode_set(mode = 'OBJECT')

    #baking!    
    bpy.ops.object.bake(type='COMBINED')

    # setting old mode

    if old_mode in modes:
        bpy.ops.object.mode_set(mode = modes[old_mode])

    # setting old uv map as active again
    if old_active_uv:
         obj.data.uv_textures.active_index = old_active_index

         
    # deletnig texture nodes
    for material in obj.data.materials:
        nodes = material.node_tree.nodes
        texture_node = nodes.get(texture_node_name)
        nodes.remove(texture_node)
        

    # saving uv file
    if not os.path.exists(baked_textures_dir):
        os.mkdir(baked_textures_dir)
    image.file_format = 'PNG'
    image.filepath_raw = baked_textures_dir + "/" + image_name + ".png"
    image.save()
    

#get scene
scene = bpy.context.scene

#list of selected objects
selected_meshes  = []
selected_objects = []

for obj in scene.objects:
    if obj.select:
        if obj.type == 'MESH':
            selected_meshes.append(obj)
        selected_objects.append(obj)
    obj.select = False
    

for obj in selected_meshes:
    scene.objects.active = obj
    obj.select = True
    print(" === Start bake_selected for object: " + obj.name + " ===")
    bake_selected()
    print(" === Stop  bake_selected for object: " + obj.name + " ===")
    obj.select = False

for obj in selected_objects:
    obj.select = True